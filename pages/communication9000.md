filters:: {"contents" false}

- This topic is about the communication between [[Server]] and the [[Tiny Runnable Programs]]
  heading:: true
-
- The idea of what the [[Server]] needs to do.
	- Get the [[dynamic commands]] : #Server #[[Tiny Runnable Programs]]  #[[dynamic commands]]
		- The [[Server]] needs to ask the Program if it has some [[dynamic commands]] on start.
		- A capable Program of [[communication9000]] needs to have one single default command. This command is only for getting the [[dynamic commands]], so the [[Server]] can register/save them.
	- Execute [[dynamic commands]] :
		- Send the name of the command with parameter to the socket file, which both Server and Program uses to communicate.
-
- Sockets should be created under /tmp/communication9000/FileName so the Program knows where to connect. And the
  id:: 62e47336-4b9c-4798-9d7c-538cd0bfb49c
-
- The idea of what the [[Tiny Runnable Programs]] needs to do.
	- Listen to the socket file for commands
	- Execute internal functions when received command.
-