- This page is about the Server and its mission
  heading:: true
-
- What does the Server do?
  background-color:: #497d46
	- The Server manages the communication between [[Client]] and [[Tiny Runnable Programs]] like a bridge or interface.
		- Between the Server and [[Tiny Runnable Programs]] the idea of [[communication9000]] is used in the Linux system with Socket files.
		- Between the Server and [[Client]] the idea of [[communication9001]] is used over the Network with TCP or UDP.
-
- What are the simple Tasks a Server should do?
  background-color:: #497d46
	- Scans for [[Tiny Runnable Programs]] in a specific folder
	- Has a [[config file]] with default settings (like for the path to tiny Programs)
		- Reads them on start
		- Reads them on a Push of a button from a command recieved
	- Execute [[Tiny Runnable Programs]] where only one Program per Folder is allowed to run.
		- To not make it complicated. Scanned and found Programs should only be executed with bash. Bash will execute binaries like normal and Shebang should do the rest if its a Script file like Bash or python.
-
- Has hardcoded [[default Commands]] defined.
  background-color:: #264c9b
	- The Server recieves the default Commands from the [[Client]] and executes internal Functions
-
-
- Communicates with a [[Client]] over a [[Network]] connection.
  background-color:: #49767b
	- The Server receives [[commands]] from the [[Client]] (the Server has [[default Commands]]  defined and the client just knows them). And sends back to the client return parameters if the job is done.
- Communicates with [[Tiny Runnable Programs]] over Socket files.
  background-color:: #49767b
	- Tries to receive a default command from the socket file.
		- Command:
	- [[communication9001]] is used as protocol to exchange the names of available [[Tiny Runnable Programs]], its status, and its [[dynamic commands]] if they have some.
	-
-
-